using System;
using NUnit.Framework;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class SocketUtilityTests
    {
        [Test]
        public void TryParseAddress_TcpAddress_Ok()
        {
            const string url = "tcp://127.0.0.1:8080";

            bool ret = SocketUtility.TryParseAddress(url, out var address, out var port, out var urlProtocol, out var urlPath);

            Assert.True(ret);
            Assert.AreEqual("127.0.0.1", address);
            Assert.AreEqual(8080, port);
            Assert.AreEqual("tcp", urlProtocol);
            Assert.AreEqual(string.Empty, urlPath);
        }


        public event Action OnClose = delegate { };
        [Test]
        public void TestEvent()
        {
            Action d = () => { };
            OnClose += d;
            OnClose();
            OnClose -= d;

            OnClose();

        }
    }
}
