using System.IO;
using NUnit.Framework;
using ProtoBuf;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class ProtobufTests
    {
        [Test]
        public void Serialize_StreamBuffer_Success()
        {
            var contact = CreateContact();

            var stream = GetSerialalizeStreamBuffer(contact);

            var memStream = GetSerialize(contact);

            Assert.AreEqual(memStream.Length, stream.Length);
            Assert.AreEqual(memStream.Position, stream.Position);
            Assert.AreEqual(memStream.Length, memStream.Position);

            CollectionAssert.AreEqual(memStream.ToArray(), stream.ToArray());
        }

        private static StreamBuffer GetSerialalizeStreamBuffer(Contact contact)
        {
            var stream = new StreamBuffer();
            Serializer.Serialize(stream, contact);
            return stream;
        }


        /// <summary>
        /// 头部预分配长度,并相等
        /// </summary>
        [Test]
        public void Serialize_PreAllocate_Success()
        {
            var contact = CreateContact();

            var stream = new StreamBuffer();
            stream.Position = 8;
            Serializer.Serialize(stream, contact);
            stream.Position = 0;
            stream.WriteBytes(0, 1, 2, 3);
            stream.WriteBytes(4, 3, 2, 1);



            var memStream = new MemoryStream();
            memStream.Position = 8;
            Serializer.Serialize(memStream, contact);
            memStream.Position = 0;
            memStream.WriteByte(0);
            memStream.WriteByte(1);
            memStream.WriteByte(2);
            memStream.WriteByte(3);

            memStream.WriteByte(4);
            memStream.WriteByte(3);
            memStream.WriteByte(2);
            memStream.WriteByte(1);

            var stanardMemStream = GetSerialize(contact);

            Assert.AreEqual(8, stream.Position);
            Assert.AreEqual(8 + stanardMemStream.Length, stream.Length);
            Assert.AreEqual(memStream.Length, stream.Length);
            Assert.AreEqual(memStream.Position, stream.Position);

            CollectionAssert.AreEqual(stanardMemStream.ToArray(), stream.ToArrayFromPos());
            CollectionAssert.AreEqual(memStream.ToArray(), stream.ToArray());
        }

        private static MemoryStream GetSerialize(Contact contact)
        {
            var stanardMemStream = new MemoryStream();
            Serializer.Serialize(stanardMemStream, contact);
            return stanardMemStream;
        }


        [Test]
        public void Deserialize_Success()
        {
            var contact = CreateContact();

            var buffer = GetSerialalizeStreamBuffer(contact);
            buffer.Position = 0;
            var dContact = Serializer.Deserialize<Contact>(buffer);

            Assert.AreEqual(contact.Name, dContact.Name);
            Assert.AreEqual(contact.Address, dContact.Address);
        }


        [Test]
        public void Deserialize_PreAllock_Success()
        {
            var contact = CreateContact();

            var buffer = new StreamBuffer();
            buffer.Position = 8;
            Serializer.Serialize(buffer, contact);
            buffer.Position = 0;
            buffer.WriteBytes(0, 1, 2, 3);
            buffer.WriteBytes(4, 3, 2, 1);

            var dContact = Serializer.Deserialize<Contact>(buffer);

            Assert.AreEqual(contact.Name, dContact.Name);
            Assert.AreEqual(contact.Address, dContact.Address);
        }


        private static Contact CreateContact()
        {
            var contact = new Contact
            {
                Name = "Jack",
                Address = "China"
            };
            return contact;
        }
    }


    [ProtoContract]
    public class Contact
    {
        [ProtoMember(1)]
        public string Name { get; set; }

        [ProtoMember(2)]
        public string Address { get; set; }
    }
}
