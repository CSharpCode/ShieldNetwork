using System;
using NUnit.Framework;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class TestCSharp
    {
        /// <summary>
        /// 测试不会设置为null,而且析构函数也不是马上执行
        /// </summary>
        [Test]
        public void DisposeClass_IsNull()
        {
            var aClass = new UseDispose();

            aClass.Dispose();
            aClass = null;
            GC.Collect();


            Assert.Null(aClass);

            // ReSharper disable once RedundantAssignment
            aClass = null;
        }


        [Test]
        public void TryCatch_RunAfter()
        {

            try
            {
                ThrowEx();
            }
            catch (Exception)
            {
#pragma warning disable 219
                int debug = 1;
#pragma warning restore 219
            }

#pragma warning disable 219
            int debug1 = 1;
#pragma warning restore 219

        }


        public void ThrowEx()
        {
            throw new Exception();
        }
    }

    internal class UseDispose : IDisposable
    {
        public string Name;

        public UseDispose()
        {
            Name = "UseDispose";
        }



        ~UseDispose()
        {
            Name = "析构";
        }


        public void Dispose()
        {
            Name = "Dispose";
        }
    }
}