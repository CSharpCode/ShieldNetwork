using NUnit.Framework;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class StreamBufferPoolTests
    {
        [Test]
        public void GetStream_SizeOk()
        {
            var stream = StreamBufferPool.GetStream(2048);

            Assert.AreEqual(0, stream.Position);
            Assert.AreEqual(0, stream.Length);
            Assert.GreaterOrEqual(2048, stream.Capacity);
        }

#if FOR_UNIT_TEST
        [Test]
        public void RleaseStream_Ok()
        {
            var stream = StreamBufferPool.GetStream(2048);
            StreamBufferPool.ReleaseStream(ref stream);

            Assert.Null(stream);

            Assert.AreEqual(1, StreamBufferPool.AvaliveBufferNum);
        }
#endif

    }
}
