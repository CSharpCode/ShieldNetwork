using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Threading;
using Hazel;
using Hazel.Tcp;
using NUnit.Framework;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class SocketTcpUnitTest
    {
        private const string SERVER_URI = "tcp://127.0.0.1:20183";


        [Test]
        [Category("集成测试")]
        public void Connect_Success()
        {
            using (var listener = new TcpConnectionListener(GetServerEndPoint()))
            {
                listener.Start();
                using (var tcp = new SocketTcp(GetSocketConfig()))
                {
                    bool isConnect = false;
                    tcp.ConnectEvent += () => { isConnect = true; };
                    var retOk = tcp.Connect();
                    Assert.IsTrue(retOk);

                    SpinWait.SpinUntil(() => isConnect, TimeSpan.FromSeconds(5));


                    Assert.AreEqual(SocketState.Connected, tcp.State);
                }
            }
        }

        private static StreamBuffer MakeStreamBuffer(byte[] data)
        {
            var length = data.Length + 4;
            var buffer = StreamBufferPool.GetStream(length);
            SocketUtility.WriteInt2Buffer(buffer.GetBuffer(), 0, length);
            buffer.Position = 4;
            buffer.Write(data, 0, data.Length);
            buffer.Position = 0;
            return buffer;
        }

        [Test]
        [Category("集成测试")]
        public void FirstData_Success()
        {
            var clientRequest = new byte[] {1, 2, 3, 4, 5, 6, 7, 8};

            var serverResponse = new byte[] {5, 6, 7, 8};


            using (var listener = new TcpConnectionListener(GetServerEndPoint()))
            {
                listener.Start();

                bool serverReceive = false;
                listener.NewConnection += (sender, e) =>
                {
                    serverReceive = true;
                    CollectionAssert.AreEqual(clientRequest, e.HandshakeData);
                    e.Connection.SendBytes(serverResponse);
                };


                using (var tcp = new SocketTcp(GetSocketConfig()))
                {
                    Assert.NotNull(tcp);

                    bool clientReceive = false;
                    tcp.ConnectEvent += () =>
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        tcp.Send(MakeStreamBuffer(clientRequest));
                    };

                    tcp.ResponseEvent += buffer =>
                    {
                        clientReceive = true;
                        CollectionAssert.AreEqual(serverResponse, buffer.ToArray());
                    };

                    var retOk = tcp.Connect();
                    Assert.IsTrue(retOk);

                    SpinWait.SpinUntil(() => serverReceive && clientReceive, TimeSpan.FromSeconds(50));
                    Assert.AreEqual(SocketState.Connected, tcp.State);

                    Assert.IsTrue(serverReceive && clientReceive);
                }
            }
        }

        [Ignore("连接外部服务器")]
        [Test]
        [Category("集成测试")]
        public void ConnectOuterServer_Success()
        {
            var data = new byte[] {1, 2, 3, 4, 5, 6, 7, 8};


            var socketConfig = new SocketConfig(ConnectionProtocol.Tcp, "tcp://10.60.80.76:20183");
            var tcp = new SocketTcp(socketConfig);
            tcp.ConnectEvent += () => { tcp.Send(MakeStreamBuffer(data)); };

            tcp.ResponseEvent += buffer =>
            {
#pragma warning disable 219
                int debug = 1;
#pragma warning restore 219
            };

            tcp.DisconnectEvent += () =>
            {
#pragma warning disable 219
                int debug = 1;
#pragma warning restore 219
            };

            tcp.ReportLogEvent += (level, s) =>
            {
#pragma warning disable 219
                int debug = 1;
#pragma warning restore 219
            };

            var retOk = tcp.Connect();
            Assert.IsTrue(retOk);

            SpinWait.SpinUntil(() => false, TimeSpan.FromSeconds(50));
            Assert.AreEqual(SocketState.Connected, tcp.State);

            tcp.Disconnect();
        }


        /// <summary>
        /// 无服务器超时,收到断开事件
        /// </summary>
        [Test]
        [Category("集成测试")]
        public void NoServer_ConnectTimeout()
        {
            using (var tcp = new SocketTcp(new SocketConfig(ConnectionProtocol.Tcp, "tcp://192.168.1.5:3214")))
            {
                var isConnect = false;
                var isDisconnect = false;
                tcp.ConnectEvent += () => { isConnect = true; };
                tcp.DisconnectEvent += () => { isDisconnect = true; };
                var retOk = tcp.Connect();
                Assert.IsTrue(retOk);

                SpinWait.SpinUntil(() => isDisconnect, TimeSpan.FromSeconds(10));

                Assert.IsTrue(isDisconnect);
                Assert.IsFalse(isConnect);
                Assert.AreEqual(SocketState.Disconnected, tcp.State);
            }
        }

        /// <summary>
        /// 发送大量数据,发送顺序正确,服务器接收顺序正确
        /// </summary>
        /// <param name="uri"></param>
        [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
        [SuppressMessage("ReSharper", "AccessToModifiedClosure")]
        [Test]
        [Category("集成测试")]
//        [TestCase("tcp://10.60.80.76:20183")]
        [TestCase(SERVER_URI)]
        public void SendBigData_Success(string uri)
        {
            var dataLength = 1024 * 100;
            var clientRequest = new byte[dataLength];
            clientRequest[dataLength - 1] = 4;

            var sendMax = 100;

            using (var listener = new TcpConnectionListener(GetServerEndPoint()))
            {
                listener.Start();

                var sendCount = 0;
                listener.NewConnection += (sender, e) =>
                {
                    e.Connection.DataReceived += (o, args) =>
                    {
                        var newBuffer = new byte[dataLength];
                        Buffer.BlockCopy(clientRequest, 0, newBuffer, 0, dataLength);
                        SocketUtility.WriteInt2Buffer(newBuffer, 0, sendCount);
                        CollectionAssert.AreEqual(newBuffer, args.Bytes);
                        sendCount++;
                    };

                    CollectionAssert.AreEqual(clientRequest, e.HandshakeData);
                    var index = SocketUtility.Convert2Int(e.HandshakeData, 0);
                    Assert.AreEqual(sendCount, index);
                    sendCount++;
                };

                using (var tcp = new SocketTcp(GetSocketConfig(uri)))
                {
                    Assert.NotNull(tcp);

                    tcp.ConnectEvent += () =>
                    {
                        for (int i = 0; i < sendMax; i++)
                        {
                            // ReSharper disable once AccessToDisposedClosure
                            var buffer = MakeStreamBuffer(clientRequest);
                            buffer.Position = 4;
                            SocketUtility.WriteInt2Buffer(buffer.GetBuffer(), 4, i);
                            tcp.Send(buffer);
                            StreamBufferPool.ReleaseStream(ref buffer);
                        }
                    };

                    var retOk = tcp.Connect();
                    Assert.IsTrue(retOk);

                    SpinWait.SpinUntil(() => sendCount == sendMax, TimeSpan.FromSeconds(200));
                    Assert.AreEqual(SocketState.Connected, tcp.State);

                    Assert.AreEqual(sendMax, sendCount);
                }
            }
        }


        [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
        [Test]
        [Category("集成测试")]
        [TestCase(SERVER_URI)]
        public void ReceiveBigData_Success(string uri)
        {
            const int serverSendMax = 100;

            var dataLength = 1024 * 100;
            var clientRequest = new byte[dataLength];
            clientRequest[dataLength - 1] = 4;

            using (var listener = new TcpConnectionListener(GetServerEndPoint()))
            {
                listener.Start();

                listener.NewConnection += (sender, e) =>
                {
                    for (int i = 0; i < serverSendMax; i++)
                    {
                        // ReSharper disable once AccessToDisposedClosure
                        var buffer = StreamBufferPool.GetStream(dataLength);
                        buffer.Write(clientRequest, 0, dataLength);
                        SocketUtility.WriteInt2Buffer(buffer.GetBuffer(), 0, i);
                        e.Connection.SendBytes(buffer.ToArray());
                        StreamBufferPool.ReleaseStream(ref buffer);
                    }
                };

                using (var tcp = new SocketTcp(GetSocketConfig(uri)))
                {
                    Assert.NotNull(tcp);

                    int clientReceiveCount = 0;
                    tcp.ResponseEvent += buffer =>
                    {
                        var newBuffer = new byte[dataLength];
                        Buffer.BlockCopy(clientRequest, 0, newBuffer, 0, dataLength);
                        SocketUtility.WriteInt2Buffer(newBuffer, 0, clientReceiveCount);
                        CollectionAssert.AreEqual(newBuffer, buffer.ToArray());
                        clientReceiveCount++;
                    };

                    tcp.ConnectEvent += () => { tcp.Send(new byte[] {0, 0, 0, 5, 1}, 0, 5); };

                    var retOk = tcp.Connect();
                    Assert.IsTrue(retOk);

                    SpinWait.SpinUntil(() => clientReceiveCount == serverSendMax, TimeSpan.FromSeconds(200));

                    Assert.AreEqual(SocketState.Connected, tcp.State);

                    Assert.AreEqual(serverSendMax, clientReceiveCount);
                }
            }
        }

        /// <summary>
        /// 多线程发送数据完整性
        /// </summary>
        /// <param name="uri"></param>
        [SuppressMessage("ReSharper", "AccessToDisposedClosure")]
        [Test]
        [Category("集成测试")]
        [TestCase(SERVER_URI)]
        public void MultiThreadSend_Sucess(string uri)
        {
            var dataLength = 1024 * 100;
            var clientRequest = new byte[dataLength];
            clientRequest[dataLength - 1] = 4;

            var sendMax = 1024;
            var threadMax = 10;

            using (var listener = new TcpConnectionListener(GetServerEndPoint()))
            {
                listener.Start();

                var receive = new List<int>();
                listener.NewConnection += (sender, e) =>
                {
                    e.Connection.DataReceived += (o, args) =>
                    {
                        var idx = SocketUtility.Convert2Int(args.Bytes, 0);
                        Assert.IsFalse(receive.Contains(idx));
                        receive.Add(idx);
                    };


                    var index = SocketUtility.Convert2Int(e.HandshakeData, 0);
                    Assert.IsFalse(receive.Contains(index));
                    receive.Add(index);
                };

                using (var tcp = new SocketTcp(GetSocketConfig(uri)))
                {
                    Assert.NotNull(tcp);

                    tcp.ConnectEvent += () =>
                    {

                        for (int i = 0; i < threadMax; i++)
                        {
                            var action = MakeSendAction(tcp, clientRequest, sendMax * i, sendMax * i + sendMax);
                            var thread = new Thread(new ThreadStart(action)) {IsBackground = true};
                            thread.Start();
                        }
                    };

                    var retOk = tcp.Connect();
                    Assert.IsTrue(retOk);

                    var isCountOk = SpinWait.SpinUntil(() => receive.Count == sendMax * threadMax, TimeSpan.FromSeconds(200));
                    receive.Sort();
                    Assert.IsTrue(isCountOk);
                    Assert.AreEqual(SocketState.Connected, tcp.State);


                    Assert.AreEqual(sendMax * threadMax, receive.Count);
                }
            }
        }

        private static Action MakeSendAction(SocketBase tcp, byte[] data, int startIndex, int endIndex)
        {
            return () =>
            {
                for (int i = startIndex; i < endIndex; i++)
                {
                    // ReSharper disable once AccessToDisposedClosure
                    var buffer = MakeStreamBuffer(data);
                    buffer.Position = 4;
                    SocketUtility.WriteInt2Buffer(buffer.GetBuffer(), 4, i);
                    tcp.Send(buffer);
                    StreamBufferPool.ReleaseStream(ref buffer);
                }
            };
        }

        private static NetworkEndPoint GetServerEndPoint()
            {
                return new NetworkEndPoint(IPAddress.Any, 20183);
            }

            private static SocketConfig GetSocketConfig(string uri = SERVER_URI)
            {
                return new SocketConfig(ConnectionProtocol.Tcp, uri);
            }
        }
    }