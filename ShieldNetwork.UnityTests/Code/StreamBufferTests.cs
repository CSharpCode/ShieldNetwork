using NUnit.Framework;

namespace ShieldNetwork.UnityTests
{
    [TestFixture]
    public class StreamBufferTests
    {
        private const int RANDOM_MAX = 9999;

        [Test]
        [TestCase(0)]
        public void StreamBuffer_SetBuf_LengthOk([Random(1, RANDOM_MAX, 1)] int buffLen)
        {
            var buf = new byte[buffLen];

            var stream = new StreamBuffer(buf);

            Assert.AreEqual(buffLen, stream.Length);
            Assert.AreEqual(0, stream.Position);
        }


        [Test]
        [TestCase(0)]
        public void ReadByte_Read_PositionOk([Random(1,RANDOM_MAX, 1)]int bufferLen)
        {
            var buffer = new byte[bufferLen];
            for (var i = 0; i < bufferLen; i++)
            {
                buffer[i] = (byte) i;
            }
            var stream = new StreamBuffer(buffer);


            for (var i = 0; i < buffer.Length; i++)
            {
                Assert.AreEqual(buffer[i], stream.ReadByte());

                Assert.AreEqual(i + 1, stream.Position);
            }

            Assert.AreEqual(buffer.Length, stream.Position);
        }


        [Test]
        public void WriteByte_WriteSome_PositionOk([Random(1, RANDOM_MAX, 1)]int len)
        {
            var stream = new StreamBuffer();

            for (int i = 0; i < len; i++)
            {
                stream.WriteByte((byte) i);
                Assert.AreEqual(i + 1, stream.Position);
                Assert.AreEqual(i + 1, stream.Length);
            }

        }
    }
}