using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Security;
using System.Threading;
using SystemSocketError = System.Net.Sockets.SocketError;

namespace ShieldNetwork
{
    public class SocketTcp : SocketBase
    {
        private const string TAG = "SocketTcp";

        private Socket mSocket;

        private readonly object mCloseSyncer;


        public SocketTcp(SocketConfig config) : base(config)
        {
            //TODO Kun 2018.04.08 为什么要使用断言而不是异常?
            Debug.Assert(0 < config.MTU);
            Debug.Assert(config.Protocol == ConnectionProtocol.Tcp);

            mCloseSyncer = new object();
            if (IsReportLog(ReportLogLevel.Debug))
            {
                DispatchReportLog(ReportLogLevel.Debug, TAG, "SocketTcp() Create");
            }
        }


        /// <summary>
        /// 连接
        /// </summary>
        public override bool Connect()
        {
            if (!base.Connect())
            {
                return false;
            }

            SetSocketState(SocketState.Connecting);
            new Thread(DnsAndConnect)
            {
                Name = "dns thread",
                IsBackground = true
            }.Start();

            return true;
        }

        private void DnsAndConnect()
        {
            try
            {
                var ipAddress = SocketUtility.GetIpAddress(ServerAddress);
                if (ipAddress == null)
                {
                    if (IsReportLog(ReportLogLevel.Error))
                    {
                        DispatchReportLog(ReportLogLevel.Error, TAG, "DnsAndConnect() failed, Invalid IPAddress:" + ServerAddress);
                    }

                    return;
                }


                mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
                {
                    NoDelay = true,
                    ReceiveTimeout = SocketConfig.Timeout,
                    SendTimeout = SocketConfig.Timeout
                };

                mSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);


                using (var timeout = new ManualResetEvent(false))
                {
                    mSocket.BeginConnect(ipAddress, ServerPort, BeginConnectCallback(timeout), mSocket);

                    timeout.WaitOne(TimeSpan.FromMilliseconds(SocketConfig.ConnectTimeout));
                    if (!mSocket.Connected)
                    {
                        throw new Exception("Connect timeout");
                    }
                }

                if (IsReportLog(ReportLogLevel.Info))
                {
                    DispatchReportLog(ReportLogLevel.Info, TAG, string.Format("DnsAndConnect(), SendBuffer:{0}, ReceiveBuffer:{1}", mSocket.SendBufferSize, mSocket.ReceiveBufferSize));
                }


                AddressResolvedAsIpv6 = SocketUtility.IsIpv6SimpleCheck(ipAddress);
                SetSocketState(SocketState.Connected);
                DispatchConnect();

                if (IsReportLog(ReportLogLevel.Info))
                {
                    DispatchReportLog(ReportLogLevel.Info, TAG, "DnsAndConnect(), connect success");
                }
            }
            catch (SecurityException ex)
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "DnsAndConnect() Connect to '" + ServerAddress + "' failed: " + ex);
                }

                Disconnect();
                return;
            }
            catch (Exception ex)
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "DnsAndConnect() Connect to '" + ServerAddress + "' failed: " + ex);
                }

                Disconnect();
                return;
            }


            new Thread(ReceiveLoop)
            {
                Name = "receive thread",
                IsBackground = true
            }.Start();
        }

        private AsyncCallback BeginConnectCallback(ManualResetEvent manualReset)
        {
            return ar =>
            {
                try
                {
                    var socket = ar.AsyncState as Socket;
                    if (socket != null)
                    {
                        //EndConnect进行同步连接,超时意发生在这里
                        socket.EndConnect(ar);
                    }
                }
                catch (Exception)
                {
                    //外边因超时等原因释放了Socket,因此可引发各种异常.
                    //各种异常都是连接失败,无须关注
                }
                finally
                {
                    manualReset.Set();
                }
            };
        }

        private void ReceiveLoop()
        {
            var headSize = SocketConfig.HeadSize;
            var headBuf = StreamBufferPool.GetStream(headSize);
            var mtu = SocketConfig.MTU;
            while (State == SocketState.Connected)
            {
                try
                {
                    headBuf.Clear();
                    DoReceive(headBuf, headSize);

                    var msgSize = SocketConfig.GetBodyLength(headBuf.GetBuffer(), 0);
                    if (msgSize <= 0)
                    {
                        throw new ApplicationException("ReceiveLoop(), msgSize error:" + msgSize);
                    }

                    if (IsReportLog(ReportLogLevel.Debug))
                    {
                        DispatchReportLog(ReportLogLevel.Debug, TAG, "message length: " + msgSize);
                    }

                    //如果断开,在进行读取直接抛出异常进行中断
                    var bodySize = msgSize - headSize;
                    if (bodySize <= 0)
                    {
                        throw new ApplicationException("ReceiveLoop(), bodySize error:" + bodySize);
                    }

                    var body = StreamBufferPool.GetStream(Math.Max(bodySize, mtu));
                    DoReceive(body, bodySize);
                    body.Position = 0;

                    DispatchResponse(body);
                }
                catch (SocketException ex)
                {
                    //只报告连接状态下的异常
                    if (State == SocketState.Connected)
                    {
                        if (IsReportLog(ReportLogLevel.Error))
                        {
                            DispatchReportLog(ReportLogLevel.Error, TAG, "ReceiveLoop() failed. SocketException: " + ex.SocketErrorCode);
                        }
                    }

                    break;
                }
                catch (Exception ex)
                {
                    if (State == SocketState.Connected)
                    {
                        if (IsReportLog(ReportLogLevel.Error))
                        {
                            DispatchReportLog(ReportLogLevel.Error, TAG, string.Concat("ReceiveLoop() issue. State: ", State, ". Server: '", ServerAddress, "' Exception: ", ex));
                        }
                    }

                    break;
                }
            }

            StreamBufferPool.ReleaseStream(ref headBuf);
            Disconnect();
        }


        /// <summary>
        /// 接收指定长度数据
        /// 接收到指定长度或抛出异常
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="size">数据长度</param>
        private void DoReceive(StreamBuffer buffer, int size)
        {
            Debug.Assert(buffer.Position == 0);
            Debug.Assert(buffer.Length == 0);
            //读取包头
            while (buffer.Position < size)
            {
                int receive;
                try
                {
                    receive = mSocket.Receive(buffer.GetBuffer(), (int) buffer.Position, (int) (size - buffer.Position), SocketFlags.None);

                    buffer.Position += receive;
                }
                catch (SocketException ex)
                {
                    //非连接状态则抛出异常
                    if (State == SocketState.Disconnecting || State == SocketState.Disconnected || ex.SocketErrorCode != SystemSocketError.WouldBlock)
                    {
                        throw;
                    }

                    //TODO Kun 2018.03.19 文章说是缓冲区满了,不应继续?
                    // http://www.cnblogs.com/tuyile006/archive/2009/09/28/1575543.html
                    if (IsReportLog(ReportLogLevel.Debug))
                    {
                        DispatchReportLog(ReportLogLevel.Debug, TAG, "DoReceive(), got a WouldBlock exception. This is non-fatal. Going to continue.");
                    }

                    continue;
                }


                if (receive == 0)
                {
                    throw new SocketException((int) SystemSocketError.ConnectionReset);
                }
            }
        }


        /// <summary>
        /// 断开连接
        /// </summary>
        public override bool Disconnect()
        {
            if (IsReportLog(ReportLogLevel.Info))
            {
                DispatchReportLog(ReportLogLevel.Info, TAG, "Disconnect()");
            }

            DoClose();

            return true;
        }

        //TODO Kun 2018.04.10 这里反复调用会如何?
        private void DoClose()
        {
            //TODO Kun 2018.04.10 重复断开测试.否则重复置为连接中又不处理
            if (State == SocketState.Disconnected || State == SocketState.Disconnecting)
            {
                return;
            }

            lock (mCloseSyncer)
            {
                if (mSocket != null)
                {
                    SetSocketState(SocketState.Disconnecting);
                    try
                    {
                        mSocket.Close(SocketConfig.CloseTimeOut);
                    }
                    catch (Exception ex)
                    {
                        if (IsReportLog(ReportLogLevel.Error))
                        {
                            DispatchReportLog(ReportLogLevel.Error, TAG, "DoClose() Exception:" + ex);
                        }
                    }

                    mSocket = null;

                    SetSocketState(SocketState.Disconnected);
                    DispatchDisconnect();
                }
            }
        }

        public override SocketError Send(byte[] data, int startIndex, int length)
        {
            //TODO Kun 2018.03.19 是否线程安全
            //TODO Kun 2018.03.19 为什么使用 mSocket.Connected 消耗大吗?
            if (mSocket == null || !mSocket.Connected)
            {
                return SocketError.Skipped;
            }

            if (data == null || data.Length == 0)
            {
                return SocketError.NoData;
            }

            if (startIndex < 0 || length <= 0)
            {
                return SocketError.NoData;
            }

            if (data.Length < startIndex + length)
            {
                return SocketError.NoData;
            }

            //NOTE Kun 2018.04.12 发送完成才返回
            //TODO Kun 2018.04.12 服务器缓存满会卡住线程
            try
            {
                var sendSize = mSocket.Send(data, startIndex, length, SocketFlags.None);
                if (sendSize != length)
                {
                    if (IsReportLog(ReportLogLevel.Error))
                    {
                        DispatchReportLog(ReportLogLevel.Error, TAG, string.Format("Send(), sendSize < length, sendSize:{0}, length:{1}", sendSize, length) );
                    }
                }
            }
            catch (Exception ex)
            {
                if (State == SocketState.Connected)
                {
                    if (IsReportLog(ReportLogLevel.Error))
                    {
                        string str = "";
                        if (mSocket != null)
                        {
                            var obj = new object[] {" Local: ", mSocket.LocalEndPoint, " Remote: ", mSocket.RemoteEndPoint, " (", mSocket.Connected ? "connected" : "not connected", ", ", mSocket.IsBound ? "bound" : "not bound", ")"};
                            str = string.Concat(obj);
                        }

                        var msg = string.Concat("Send() failed, to: ", ServerAddress, ". Uptime: ", SocketUtility.GetTickCount() - SocketConfig.StartupTime, "ms.", AddressResolvedAsIpv6 ? " IPv6" : "", str, " ex:", ex);
                        DispatchReportLog(ReportLogLevel.Error, TAG, msg);
                    }

                    Disconnect();
                }


                return SocketError.Exception;
            }

            return SocketError.Success;
        }


        public override void Dispose()
        {
            DoClose();
        }
    }
}