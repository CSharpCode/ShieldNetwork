using System;
using System.Net;
using System.Net.Sockets;

namespace ShieldNetwork
{
    public static class SocketUtility
    {
        private static readonly Func<int> sIntegerMilliseconds = () => Environment.TickCount;


        public static int GetTickCount()
        {
            return sIntegerMilliseconds();
        }


        public static bool IsIpv6SimpleCheck(IPAddress address)
        {
            return address != null && address.ToString().Contains(":");
        }

        public static bool TryParseAddress(string url, out string address, out ushort port, out string urlProtocol, out string urlPath)
        {
            address = string.Empty;
            port = 0;
            urlProtocol = string.Empty;
            urlPath = string.Empty;
            var str = url;
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            //取得协议,如tcp://
            int index = str.IndexOf("://", StringComparison.Ordinal);
            if (index >= 0)
            {
                urlProtocol = str.Substring(0, index);
                str = str.Substring(index + 3);
            }

            //url_path,如tcp://127.0.0.1:8080/url_path
            index = str.IndexOf("/", StringComparison.Ordinal);
            if (index >= 0)
            {
                urlPath = str.Substring(index);
                str = str.Substring(0, index);
            }

            index = str.LastIndexOf(':');
            if (index < 0)
            {
                return false;
            }

            //只有1个":"
            if ((str.IndexOf(':') != index) && (!str.Contains("[") || !str.Contains("]")))
            {
                return false;
            }

            //地址":"端口
            address = str.Substring(0, index);
            return UInt16.TryParse(str.Substring(index + 1), out port);
        }

        public static IPAddress GetIpAddress(string address)
        {
            IPAddress ret;
            //解析出address则返回
            if (!IPAddress.TryParse(address, out ret))
            {
                //用dns解析地址,找到第一个可用的地址,V6优先
                var addressList = Dns.GetHostEntry(address).AddressList;
                //找到第一个
                foreach (var t in addressList)
                {
                    if (t.AddressFamily == AddressFamily.InterNetworkV6)
                    {
                        return t;
                    }

                    if (ret == null && t.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ret = t;
                    }
                }
            }

            return ret;
        }

        public static int Convert2Int(byte[] buffer, int startIndex, bool isBigEndian = true)
        {
            if (buffer == null) throw new ArgumentNullException("buffer");
            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException("startIndex", "Convert2Int(), startIndex < 0");
            }

            if (buffer.Length - startIndex < 4)
            {
                throw new ArgumentException("Convert2Int(), available buf < 4");
            }

            if (isBigEndian)
                return buffer[startIndex] << 24 | buffer[startIndex + 1] << 16 | buffer[startIndex + 2] << 8 | buffer[startIndex + 3];

            return buffer[startIndex] | buffer[startIndex + 1] << 8 | buffer[startIndex + 2] << 16 | buffer[startIndex + 3] << 24;
        }


        public static void WriteInt2Buffer(byte[] buffer, int startIndex, int num, bool isBigEndian = true)
        {
            if (buffer == null) throw new ArgumentNullException("buffer");
            if (startIndex < 0)
            {
                throw new ArgumentOutOfRangeException("startIndex", "WriteInt2StreamBuffer(), startIndex < 0");
            }

            if (buffer.Length - startIndex < 4)
            {
                throw new ArgumentException("WriteInt2StreamBuffer(), available buf < 4");
            }

            byte a = (byte) (num & 0xff);
            byte b = (byte) (num >> 8 & 0xff);
            byte c = (byte) (num >> 16 & 0xff);
            byte d = (byte) (num >> 24 & 0xff);
            if (isBigEndian)
            {
                buffer[startIndex] = d;
                buffer[startIndex + 1] = c;
                buffer[startIndex + 2] = b;
                buffer[startIndex + 3] = a;
            }
            else
            {
                buffer[startIndex] = a;
                buffer[startIndex + 1] = b;
                buffer[startIndex + 2] = c;
                buffer[startIndex + 3] = d;
            }
        }
    }
}