#define PLAT_NO_INTERLOCKED

using System.Diagnostics;

namespace ShieldNetwork
{

    //TODO Kun 2018.03.18 动态改变Buffer
    //TODO Kun 2018.03.18 依据使用频率删除Buffer
    //TODO Kun 2018.03.18 由小到大排列buffer,优先使用小Buffer,减少总分配量

    /// <summary>
    /// StreamBuffer池
    /// </summary>
    public static class StreamBufferPool
    {
        private const int POOL_SIZE = 20;
        private static readonly StreamBuffer[] sPool = new StreamBuffer[POOL_SIZE];

#if FOR_UNITY_TEST

        public static int AvaliveBufferNum
        {
            get
            {
                int count = 0;
                lock (sPool)
                {
                    foreach (var buffer in sPool)
                    {
                        if (buffer != null)
                        {
                            count++;
                        }

                    }
                }

                return count;
            }
        }
#endif

        public static StreamBuffer GetStream(int size)
        {
            Debug.Assert(0 < size);

#if PLAT_NO_INTERLOCKED
            lock(sPool)
            {
                //返回适合大小的
                for (var i = 0; i < sPool.Length; i++)
                {
                    var v = sPool[i];
                    if (v != null && size < v.Capacity)
                    {
                        sPool[i] = null;
                        return v;
                    }
                }

                //返回池中的,减少创建
                for (var i = 0; i < sPool.Length; i++)
                {
                    var v = sPool[i];
                    if (v != null)
                    {
                        sPool[i] = null;
                        v.SetCapacityMinimum(size);
                        return v;
                    }
                }
            }
#else
            object tmp;
            for (var i = 0; i < sPool.Length; i++)
            {
                if ((tmp = Interlocked.Exchange(ref sPool[i], null)) != null) return (byte[])tmp;
            }
#endif
            return new StreamBuffer(size);
        }


        public static void ReleaseStream(ref StreamBuffer stream)
        {
            Debug.Assert(stream != null);

            if (stream == null) return;
            stream.Clear();

#if PLAT_NO_INTERLOCKED
            lock (sPool)
            {
                for (var i = 0; i < sPool.Length; i++)
                {
                    if (sPool[i] == null)
                    {
                        sPool[i] = stream;
                        break;
                    }
                }
            }
#else
            for (var i = 0; i < sPool.Length; i++)
            {
                if (Interlocked.CompareExchange(ref sPool[i], stream, null) == null)
                {
                    break; // found a null; swapped it in
                }
            }
#endif
            // if no space, just drop it on the floor
            stream = null;
        }

        public static void Flush()
        {
#if PLAT_NO_INTERLOCKED
            lock(sPool)
            {
                for (var i = 0; i < sPool.Length; i++) sPool[i] = null;
            }
#else
            for (var i = 0; i < sPool.Length; i++)
            {
                Interlocked.Exchange(ref sPool[i], null); // and drop the old value on the floor
            }
#endif
        }
    }
}
