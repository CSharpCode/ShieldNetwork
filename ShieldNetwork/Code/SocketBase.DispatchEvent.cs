using System;

namespace ShieldNetwork
{
    // ReSharper disable once InconsistentNaming
    public abstract partial class SocketBase
    {
        public event Action ConnectEvent = delegate { };
        public event Action DisconnectEvent = delegate { };
        public event Action<StreamBuffer> ResponseEvent = delegate { };
        public event Action<ReportLogLevel, string> ReportLogEvent = delegate { };

        protected void DispatchConnect()
        {
            try
            {
                ConnectEvent();
            }
            catch (Exception ex)
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "OnConnect Exception:" + ex);
                }
            }
        }

        protected void DispatchDisconnect()
        {
            try
            {
                DisconnectEvent();
            }
            catch (Exception ex)
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "OnDisconnect() Exception:" + ex);
                }
            }
        }

        protected bool IsReportLog(ReportLogLevel level)
        {
            return SocketConfig.LogLevel <= level;
        }

        protected void DispatchReportLog(ReportLogLevel level, string tag, string msg)
        {
            try
            {
                ReportLogEvent(level, string.Concat(tag, " ", msg));
            }
            catch (Exception)
            {
                //TODO Kun 2018.04.09 递归调用如何处理?
//                if (IsReportLog(ReportLogLevel.ERROR))
//                {
//                    OnReportLog(ReportLogLevel.ERROR, TAG, "OnReportLog() Exception:" + ex);
//                }
            }
        }

        protected void DispatchResponse(StreamBuffer buffer)
        {
            try
            {
                ResponseEvent(buffer);
            }
            catch (Exception ex)
            {
                //报告并忽略逻辑层异常
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "OnResponse() Exception:" + ex);
                }
            }
        }
    }
}