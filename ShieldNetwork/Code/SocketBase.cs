using System;

namespace ShieldNetwork
{
    // ReSharper disable once InconsistentNaming
    public abstract partial class SocketBase : IDisposable
    {
        private const string TAG = "SocketBase";

        protected SocketConfig SocketConfig { get; private set; }


        public string ServerAddress { get; private set; }

        public ushort ServerPort { get; private set; }

        public string UrlProtocol { get; private set; }

        public bool AddressResolvedAsIpv6 { get; protected set; }

        private SocketState mState;

        public SocketState State
        {
            get { return mState; }
        }


        protected SocketBase(SocketConfig socketConfig)
        {
            SocketConfig = socketConfig;
        }

        public virtual bool Connect()
        {
            if (SocketState.Disconnected != mState)
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "Connect() failed:connection in state:" + mState);
                }

                return false;
            }


            string address;
            ushort port;
            //TODO Kun 2018.04.10 放到SocketConfig中更合适?
            //TODO Kun 2018.04.09 把解析放到ServerAddress中更合适?
            //TODO Kun 2018.04.09 从地址中解析协议更合适?
            string urlProtocol;
            string urlPath;
            if (!SocketUtility.TryParseAddress(SocketConfig.ServerAddress, out address, out port, out urlProtocol, out urlPath))
            {
                if (IsReportLog(ReportLogLevel.Error))
                {
                    DispatchReportLog(ReportLogLevel.Error, TAG, "Connect() Failed parsing address:" + SocketConfig.ServerAddress);
                }

                return false;
            }

            ServerAddress = address;
            ServerPort = port;
            UrlProtocol = urlProtocol;

            if (IsReportLog(ReportLogLevel.Debug))
            {
                var objs = new object[]
                    {"Connect() ", ServerAddress, ":", ServerPort, " Protocol: ", SocketConfig.Protocol};
                DispatchReportLog(ReportLogLevel.Debug, TAG, string.Concat(objs));
            }

            return true;
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        public abstract bool Disconnect();

        /// <summary>
        ///发送数据
        /// </summary>
        /// <param name="buffer">发送buffer内全部内容</param>
        /// <returns></returns>
        public SocketError Send(StreamBuffer buffer)
        {
            if (buffer == null || buffer.Length == 0)
            {
                return SocketError.NoData;
            }

            return Send(buffer.GetBuffer(), 0, (int) buffer.Length);
        }

        /// <summary>
        ///发送数据
        /// </summary>
        /// <param name="data"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public abstract SocketError Send(byte[] data, int startIndex, int length);


        protected void SetSocketState(SocketState state)
        {
            //TODO Kun 2018.03.19 可能多线程操作,使用线程安全
            mState = state;
        }

        public abstract void Dispose();
    }
}