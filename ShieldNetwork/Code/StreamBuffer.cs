using System;
using System.IO;

namespace ShieldNetwork
{
    //TODO Kun 2018.03.18 很有可能在多线程中操作
    public class StreamBuffer : Stream
    {
        private const int DEFAULT_INITIAL_SIZE = 0;

        private byte[] mBuf;

        //实际容量
        private int mLen;

        //当前位置
        private int mPos;


        public StreamBuffer(int size = DEFAULT_INITIAL_SIZE)
        {
            mBuf = new byte[size];
        }

        public StreamBuffer(byte[] buf)
        {
            mBuf = buf;
            mLen = buf.Length;
        }


        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return true; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        /// <summary>
        /// 实际内容长度,字节
        /// </summary>
        public override long Length
        {
            get { return mLen; }
        }

        public override long Position
        {
            get { return mPos; }
            set
            {
                mPos = (int) value;
                if (mLen < mPos)
                {
                    mLen = mPos;
                    ExtendSize(mLen);
                }
            }
        }

        public int Capacity
        {
            get { return mBuf != null ? mBuf.Length : 0; }
        }

        /// <summary>
        /// 扩展容量
        /// 小于目标容量就扩展
        /// </summary>
        /// <param name="size"></param>
        private void ExtendSize(int size)
        {
            if (size <= mBuf.Length)
            {
                return;
            }

            var length = mBuf.Length;
            if (length == 0)
            {
                length = 1;
            }

            //扩大缓存约为目标的两倍以内
            while (length < size)
            {
                length *= 2;
            }

            var dst = new byte[length];
            Buffer.BlockCopy(mBuf, 0, dst, 0, mBuf.Length);
            mBuf = dst;
        }

        /// <summary>
        /// 压缩头部空白位置
        /// </summary>
        public void Compact()
        {
            var num = Length - Position;
            if (0L < num)
            {
                Buffer.BlockCopy(mBuf, (int) Position, mBuf, 0, (int) num);
            }

            Position = 0L;
            SetLength(num);
        }

        public override void Flush() { }

        public byte[] GetBuffer()
        {
            return mBuf;
        }


        /// <summary>
        /// 取得当前位置并扩展
        /// </summary>
        /// <param name="length"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public byte[] GetBufferAndAdvance(int length, out int offset)
        {
            offset = (int) Position;
            Position += length;
            return mBuf;
        }

        //
        public override int Read(byte[] buffer, int offset, int count)
        {
            if (buffer == null) throw new ArgumentNullException("buffer", "ArgumentNull_Buffer");
            if (offset < 0) throw new ArgumentOutOfRangeException("offset", "ArgumentOutOfRange_NeedNonNegNum");
            if (count < 0) throw new ArgumentOutOfRangeException("count", "ArgumentOutOfRange_NeedNonNegNum");
            if (buffer.Length < offset + count) throw new ArgumentException("Argument_InvalidOffLen");

            //TODO 加入buffer为空,长度检测
            var num = mLen - mPos;
            if (num <= 0)
            {
                return 0;
            }

            //TODO 修改参数不好吧,新建变量?
            if (num < count)
            {
                count = num;
            }

            Buffer.BlockCopy(mBuf, mPos, buffer, offset, count);
            mPos += count;
            return count;
        }

        public override int ReadByte()
        {
            //-1代表无效值
            if (mLen <= mPos)
            {
                return -1;
            }

            return mBuf[mPos++];
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            int num;
            switch (origin)
            {
                case SeekOrigin.Begin:
                    num = (int) offset;
                    break;

                case SeekOrigin.Current:
                    num = mPos + ((int) offset);
                    break;

                case SeekOrigin.End:
                    num = mLen + ((int) offset);
                    break;

                default:
                    throw new ArgumentException("Invalid seek origin");
            }

            if (num < 0)
            {
                throw new ArgumentException("Seek before begin");
            }

            if (mLen < num)
            {
                throw new ArgumentException("Seek after end");
            }

            mPos = num;
            return mPos;
        }

        public void SetCapacityMinimum(int neededSize)
        {
            ExtendSize(neededSize);
        }

        /// <summary>
        /// 修改内容长度
        /// 设置为0,则可重用Stream
        /// </summary>
        /// <param name="value"></param>
        public override void SetLength(long value)
        {
            if (value < 0) throw new ArgumentOutOfRangeException("value");

            mLen = (int) value;
            ExtendSize(mLen);
            //Buffer变小,设置为最后位置
            if (mLen < mPos)
            {
                mPos = mLen;
            }
        }

        /// <summary>
        /// 重置位置与长度,重用缓存
        /// </summary>
        public void Clear()
        {
            mPos = 0;
            mLen = 0;
        }

        public byte[] ToArray()
        {
            var dst = new byte[mLen];
            Buffer.BlockCopy(mBuf, 0, dst, 0, mLen);
            return dst;
        }

        public byte[] ToArrayFromPos()
        {
            var count = mLen - mPos;
            if (count <= 0)
            {
                return new byte[0];
            }

            var dst = new byte[count];
            Buffer.BlockCopy(mBuf, mPos, dst, 0, count);
            return dst;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (buffer == null) throw new ArgumentNullException("buffer", "ArgumentNull_Buffer");
            if (offset < 0) throw new ArgumentOutOfRangeException("offset", "ArgumentOutOfRange_NeedNonNegNum");
            if (count < 0) throw new ArgumentOutOfRangeException("count", "ArgumentOutOfRange_NeedNonNegNum");
            if (buffer.Length < offset + count) throw new ArgumentException("Argument_InvalidOffLen");


            var size = mPos + count;
            ExtendSize(size);
            if (mLen < size)
            {
                mLen = size;
            }

            Buffer.BlockCopy(buffer, offset, mBuf, mPos, count);
            mPos = size;
        }

        public override void WriteByte(byte value)
        {
            var num = mPos + 1;
            if (mLen < num)
            {
                mLen = num;
                ExtendSize(mLen);
            }

            mBuf[mPos++] = value;
        }

        public void WriteBytes(byte v0, byte v1)
        {
            var num = mPos + 2;
            if (mLen < num)
            {
                mLen = num;
                ExtendSize(mLen);
            }

            mBuf[mPos++] = v0;
            mBuf[mPos++] = v1;
        }

        public void WriteBytes(byte v0, byte v1, byte v2)
        {
            var num = mPos + 3;
            if (mLen < num)
            {
                mLen = num;
                ExtendSize(mLen);
            }


            mBuf[mPos++] = v0;
            mBuf[mPos++] = v1;
            mBuf[mPos++] = v2;
        }

        public void WriteBytes(byte v0, byte v1, byte v2, byte v3)
        {
            var num = mPos + 4;
            if (mLen < num)
            {
                mLen = num;
                ExtendSize(mLen);
            }


            mBuf[mPos++] = v0;
            mBuf[mPos++] = v1;
            mBuf[mPos++] = v2;
            mBuf[mPos++] = v3;
        }
    }
}