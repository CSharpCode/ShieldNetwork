namespace ShieldNetwork
{
    public enum ConnectionProtocol : byte
    {
        Tcp = 1,
        Udp = 0,
    }
}