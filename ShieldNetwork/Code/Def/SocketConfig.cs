namespace ShieldNetwork
{
    public struct SocketConfig
    {
        /// <summary>
        /// 默认超时时间
        /// </summary>
        private const int TIMEOUT_DEFAULT = 10000;

        /// <summary>
        /// 默认MTU长度
        /// </summary>
        private const int MTU_DEFAULT = 1200;

        /// <summary>
        /// 默认关闭超时时间
        /// </summary>
        private const int CLOSE_TIME_OUT_DEFAULT = 500;

        /// <summary>
        /// 默认头部长度字节数
        /// </summary>
        private const int HEADSIZE_DEFAULT = 4;

        /// <summary>
        /// 默认编码方式
        /// </summary>
        private const bool IS_BIG_ENDIAN_DEFAULT = true;

        /// <summary>
        /// 默认Log等级
        /// </summary>
        private const ReportLogLevel REPORT_LOG_LEVEL_DEFAULT = ReportLogLevel.Warning;

        /// <summary>
        /// 默认连接超时
        /// </summary>
        private const int CONNECT_TIMEOUT = 5000;

        public SocketConfig(ConnectionProtocol protocol, string serverAddress) : this()
        {
            Protocol = protocol;
            ServerAddress = serverAddress;

            Timeout = TIMEOUT_DEFAULT;
            MTU = MTU_DEFAULT;

            StartupTime = SocketUtility.GetTickCount();

            CloseTimeOut = CLOSE_TIME_OUT_DEFAULT;

            HeadSize = HEADSIZE_DEFAULT;

            IsBigEndian = IS_BIG_ENDIAN_DEFAULT;

            LogLevel = REPORT_LOG_LEVEL_DEFAULT;

            ConnectTimeout = CONNECT_TIMEOUT;
        }

        /// <summary>
        /// 协议
        /// </summary>
        public ConnectionProtocol Protocol { get; private set; }


        /// <summary>
        /// 地址
        /// </summary>
        public string ServerAddress { get; private set; }

        /// <summary>
        /// 消息头长度.
        /// 固定4字节
        /// </summary>
        public int HeadSize { get; private set; }

        /// <summary>
        /// 解析消息长度时选择大小端方法.
        /// 默认大端
        /// </summary>
        public bool IsBigEndian { get; set; }

        /// <summary>
        /// 每次接收数据缓存大小.字节
        /// 默认1200
        /// </summary>
        public int MTU { get; private set; }


        /// <summary>
        /// 发送和接收超时.毫秒
        /// 默认10000毫秒
        /// </summary>
        public int Timeout { get; set; }


        /// <summary>
        /// Socket关闭超时,毫秒
        /// 默认500毫秒
        /// </summary>
        public int CloseTimeOut { get; set; }

        /// <summary>
        /// SocketBase启动时间,毫秒
        /// </summary>
        public int StartupTime { get; private set; }

        /// <summary>
        /// Log等级
        /// 默认 ReportLogLevel.Warning
        /// </summary>
        public ReportLogLevel LogLevel { get; set; }

        /// <summary>
        /// 连接超时,毫秒
        /// 默认 5000
        /// </summary>
        public int ConnectTimeout { get; set; }

        public int GetBodyLength(byte[] buffer, int startIndex)
        {
            return SocketUtility.Convert2Int(buffer, startIndex, IsBigEndian);
        }
    }
}