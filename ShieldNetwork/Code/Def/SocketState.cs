namespace ShieldNetwork
{
    public enum SocketState
    {
        Disconnected,
        Connecting,
        Connected,
        Disconnecting,
    }
}
