namespace ShieldNetwork
{
    public enum ReportLogLevel : byte
    {
        /// <summary>
        /// 相当于全部显示
        /// </summary>
        Debug = 0,

        Info = 1,

        Warning = 2,

        Error = 3,

        None = 4,
    }
}