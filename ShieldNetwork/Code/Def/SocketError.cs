namespace ShieldNetwork
{
    public enum SocketError
    {
        Success,
        Skipped,
        NoData,
        Exception
    }
}